const fs = require("fs");
const { ethers } = require("ethers");
const config = require("./config");

let rpcIndex = 0; // 当前RPC节点的索引

config.walletList.forEach((item) => {
  const { privateKey, toAddress } = item;
  main(privateKey, toAddress);
});

async function main(privateKey, toAddress) {
  let provider; // 将provider声明在外部，以便在函数间共享
  let wallet;

  // 初始化并连接到第一个RPC节点
  connectToNextRpc();

  // 获取钱包地址
  const address = await wallet.getAddress();
  console.log(`Wallet address: ${address}`);
  // 当前的nonce
  let currentNonce = await getCurrentNonce();

  let successCount = 0; // 成功次数累计（成功就会累加）
  let errorCount = 0; // 连续错误次数统计（成功一次清0）

  // 停止标记
  let stopFlag = false;

  // 连接到下一个RPC节点
  function connectToNextRpc() {
    if (rpcIndex < config.rpcUrls.length) {
      const rpcUrl = config.rpcUrls[rpcIndex];
      console.log(`尝试连接到RPC节点：${rpcUrl}`);
      // 连接节点
      provider = new ethers.providers.JsonRpcProvider(rpcUrl);
      // 创建钱包
      wallet = new ethers.Wallet(privateKey.trim(), provider);
      // 下个节点索引+1
      rpcIndex++;
    } else {
      console.error("所有RPC节点都无法连接,正在重新尝试...");
      rpcIndex = 0;
    }
  }

  // 转成16进制
  const convertToHex = (str = "") => {
    return ethers.utils.hexlify(ethers.utils.toUtf8Bytes(str));
  };

  // 获取当前账户的 nonce
  async function getCurrentNonce() {
    const nonce = await wallet.getTransactionCount();
    console.log("Nonce:", nonce);
    return nonce;
  }

  // 获取当前主网 gas 价格
  async function getGasPrice() {
    const gasPrice = await provider.getGasPrice();
    return gasPrice;
  }

  // 获取链上实时 gasLimit
  async function getGasLimit(hexData) {
    try {
      const gasLimit = await provider.estimateGas({
        to: toAddress || address,
        value: ethers.utils.parseEther("0"),
        data: hexData,
      });
      return gasLimit.toNumber();
    } catch (error) {
      console.error("Error estimating gas limit:", error.message);
      return config.defaultGasLimit; // 使用默认的gasLimit作为后备
    }
  }

  // 监听交易状态
  function watchTransaction(tx, nonce, needMoreGas) {
    provider
      .waitForTransaction(tx.hash, 1, config.transactionConfirmationTimeout)
      .then((receipt) => {
        if (receipt.status === 0) {
          console.error(
            `【ErrorCount:${errorCount} Nonce: ${nonce}】交易失败，交易Hash：${tx.hash}`
          );
          errorCount++;
        } else {
          console.log(
            `【SuccessCount:${successCount} Nonce: ${nonce}】交易成功被确认，交易Hash：${tx.hash}`
          );

          // 将交易收据转换为字符串并追加到文件
          const receiptString = JSON.stringify({ nonce, receipt: tx }, null, 2);
          fs.appendFileSync("transactionReceipt.txt", receiptString + "\n\n");

          successCount++;
          errorCount = 0; // 重置错误计数

          // 获取余额（以wei为单位）
          provider.getBalance(address).then((balance) => {
            // ethers.js 中的 BigNumber 对象可以转换为字符串
            console.log(
              `【余额 ${address}】: ${ethers.utils.formatEther(balance)} ETH`
            );
          });
        }
      })
      .catch(() => {
        if (!needMoreGas) {
          console.error(
            `【Warn Nonce: ${nonce}】交易未在预期时间内被打包，尝试使用Gas【${config.increaseGas}】倍价格重新发送`
          );
          sendTransaction(true, nonce); // 递增Gas价格并重新发送交易
        }
      });
  }

  // 转账交易
  async function sendTransaction(needMoreGas = false, resetNonce) {
    const sendNonce = resetNonce || currentNonce;
    const tokenJson = config.tokenJson.trim()
    try {
      // 铭文数据处理
      const hexData = config.noChangeHexData ? tokenJson : convertToHex(tokenJson);
      // 获取实时 gasPrice
      const currentGasPrice = await getGasPrice();
      const weiAmount = ethers.BigNumber.from(currentGasPrice);
      // 使用 formatEther 方法将 wei 转换为 ETH
      const ethAmount = ethers.utils.formatEther(weiAmount);
      console.log(
        `当前GasPrice:【${currentGasPrice}】gwei 约等于 ${ethAmount} ETH`
      );
      // 判断是否在当前 gasPrice 上增加一定倍数
      const gasPrice = needMoreGas
        ? ethers.utils.hexlify(
            currentGasPrice
              .mul(ethers.BigNumber.from(config.increaseGas * 100))
              .div(ethers.BigNumber.from(100))
          )
        : currentGasPrice;
      // 获取当前钱包的 gasLimit 限制设置
      let gasLimit = await getGasLimit(hexData);
      gasLimit += config.gasLimitFloatAdd;

      // 如果有手动设置的gasLimitMax
      if (config.gasLimitMax && gasLimit > config.gasLimitMax) {
        console.log(
          `【Error】当前的gas过高${gasLimit}，超出config设置的gasLimitMax限制，脚本自动退出，请重新设置后再运行`
        );
        stopFlag = true;
        return;
      }

      const transaction = {
        to: toAddress || address,
        value: ethers.utils.parseEther("0"),
        data: hexData,
        nonce: sendNonce,
        gasPrice,
        gasLimit,
      };

      console.log("发送交易数据：", transaction);
      const tx = await wallet.sendTransaction(transaction);
      console.log(
        `【Info】发送交易成功：Transaction with nonce ${sendNonce} hash:`,
        tx.hash
      );

      watchTransaction(tx, currentNonce, needMoreGas); // 监听交易状态
    } catch (error) {
      console.log("error.code", error.code);
      if (error.code === "INSUFFICIENT_FUNDS") {
        console.log("【Warn】余额不足");
        config.stopNoMoney && (stopFlag = true);
      } else if (error.code === "REPLACEMENT_UNDERPRICED") {
        console.log(
          "【Warn】重置的gas费用还是太低，请调整increaseGas，或等待下一次交易"
        );
      } else if (error.code === "NONCE_EXPIRED") {
        console.log("【Warn】nonce 过期，已被使用");
        currentNonce = await getCurrentNonce();
      } else if (
        error.code === "SERVER_ERROR" ||
        error.code === "NETWORK_ERROR" ||
        error.code === "TIMEOUT"
      ) {
        console.log("【Warn】RPC节点连接网络错误或超时，正在尝试更换节点...");
        connectToNextRpc(); // 尝试连接到下一个RPC节点
      } else {
        console.error(`【Error ${sendNonce}】:`, error.message);
      }
    } finally {
      // 无论成功还是失败，都检查是否需要继续发送交易
      if (
        !needMoreGas &&
        !stopFlag &&
        successCount < config.repeatCount &&
        errorCount < config.errorTimeLimit
      ) {
        config.waitTime &&
          console.log(`等待下一个交易时间：${config.waitTime / 1000} s`);

        setTimeout(() => {
          currentNonce++;
          sendTransaction(); // 递增 nonce 以发送下一个交易
        }, config.waitTime);
      }
    }
  }

  // 初始化并发送交易
  async function init() {
    try {
      // 开始发送交易
      sendTransaction();
    } catch (error) {
      console.error("初始化失败：", error.message);
    }
  }

  init();
}
