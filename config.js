const config = {
  // 你想要打多少张，这里就设置多少
  repeatCount: 999999,

  walletList: [
    {
      // 你钱包的私钥
      privateKey: "",
      // 发送方地址（不填默认就是打给自己）
      toAddress: "0x02a0197781ed6d4fe5e0b36599fc4fda812a5817",
    },
  ],

  // 铭文json数据（替换成你想打的铭文json格式数据）
  // tokenJson: 'data:,{"p":"bsc-20","op":"mint","tick":"bsci","amt":"1000"}',
  // tokenJson: 'data:,{"p":"asc-20","op":"mint","tick":"avav","amt":"69696969"}',
  // tokenJson: 'data:,{"p":"fair-20","op":"mint","tick":"fair","amt":"1000"}',
  tokenJson: "0x1249c58b",
  noChangeHexData: false,

  // RPC节点列表（兼容 evm 链都行）打哪条链就用哪条链的节点地址
  // eth =>  https://mainnet.infura.io/v3
  // arb => https://arb1.arbitrum.io/rpc
  // polygon => https://polygon-rpc.com
  // op => https://mainnet.optimism.io
  // linea => https://mainnet.infura.io/v3
  // scroll => https://rpc.scroll.io
  // zks => https://mainnet.era.zksync.io
  // cfx => https://evm.confluxrpc.com
  // avav => https://avalanche.public-rpc.com
  // arb => https://avalanche.public-rpc.com
  // wss://arbitrum-one.publicnode.com
  rpcUrls: ["https://evm-1.nodeinfo.cc", "https://evm-gatenode.cc"],
  // rpcUrl: "https://arbitrum.llamarpc.com",
  // rpcUrl: "wss://arbitrum-one.publicnode.com",

  // 交易超时未被广播后，在当前的 gas费用基础上增加多少倍, 增加mint成功率，1就是*100%不增加，若设置1.2 即增加120%，也就是比原来增加了20%
  increaseGas: 1.3,

  // 交易超时未被广播的等待时间（毫秒），会以increaseGas的gas费用重新发起交易
  transactionConfirmationTimeout: 30000,

  // mint 连续错误次数限制10次（超过后脚本自动停止）
  errorTimeLimit: 10,

  //最高不超过gas(用于限制mint gas最大数量，防止超支)
  gasLimitMax: 0,

  // 若获取不到当前gasLimit使用默认的gasLimit作为后备
  defaultGasLimit: 80000,

  //添加浮动gas
  gasLimitFloatAdd: 10000,

  // 区块高度上限 0为不设置高度上限（一般项目方有规定数量从什么区块开始，什么区块结束, 如果是合约没有高度这一说）
  blockNumberLimitMax: 0,

  // mint成功后继续下一个mint的间隔时间（毫秒），如果间隔太短不上链需要设置
  waitTime: 0,

  // 余额不足是否停止mint
  stopNoMoney: false,
};

module.exports = config;
